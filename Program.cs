﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3SAT
{
    class Program
    {
        static void Main(string[] args)
        {
            var reader = new StreamReader(new FileStream(args.Last(), FileMode.Open, FileAccess.Read));
            TSatProblem prob = TSatProblem.FromText(reader);
            reader.Close();
            //BruteforceSolver slv = new BruteforceSolver();
            //double res = slv.Solve(prob);

            SatSolver solver = new SatSolver(prob);
            double avg = 0;
            for (int i = 0; i < 10; i++)
            {
                //avg += solver.Solve(prob.CCount());
                avg = solver.Solve(2*prob.CCount());
            }
            //Console.Out.WriteLine((res-avg)/res);
            //Console.WriteLine(avg/1);
            int ITERATIONS = 5000;

            /*for (int j = 20; j < 40; j+=20)
            {
                int avg = 0;
                int correct = 0;
                for (int l = 0; l < 20; l++)
                {
                    var result = solver.Solve(prob.CCount()*2, ITERATIONS, 5, false, 90);
                    if (result != 5000) {
                        avg +=result;
                        correct++;
                    }
                }
                    Console.WriteLine(correct);
            }*/

        }

        static void Test()
        {
            int [] maxs = new int[4];
            TSatProblem [] problems = new TSatProblem[4];

            maxs[0] = 224;
            maxs[1] = 212;
            maxs[2] = 218;
            
            TestPop(problems,maxs); 
        }

        static void TestPop(TSatProblem[] problems, int [] maxs)
        {
            
            for (int i = 1; i < 15; i++)
            {
                int res = 0;
                for (int j = 0; j < 20; j++)
                {
                    var x = 0;
                    foreach (var prob in problems)
                    {
                        SatSolver solver = new SatSolver(prob);
                        var result = solver.Solve(prob.Size/5*i, 5000, 5,false, maxs[x]);
                        res += result;
                        x++;
                    }
                }
                Console.WriteLine(res/(problems.Length*10));
            }

        }

        }
}
