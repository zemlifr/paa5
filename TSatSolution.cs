﻿using System;

namespace _3SAT
{
    public class TSatSolution
    {
        private bool[] values;
        private bool validity;
        private int fitness;
        private int weight;
        static Random rand  = new Random(DateTime.Now.Millisecond);

        public TSatSolution(bool[] vals)
        {
            values = vals;
            fitness = 0;
            validity = false;
            weight = 0;
        }

        public TSatSolution(int lenght)
        {
            values = new bool[lenght];
            for (int j = 0; j < values.Length; j++)
            {
                var x = rand.NextDouble();
                values[j] = x >= 0.5;
            }

            fitness = 0;
            validity = false;
            weight = 0;
        }

        public bool[] Values => values;

        public bool Validity
        {
            get { return validity; }
            set { validity = value; }
        }

        public int Fitness
        {
            get { return fitness; }
            set { fitness = value; }
        }

        public int Weight
        {
            get { return weight; }
        }

        public void ComputeFitness(TSatProblem prob)
        {
            weight = 0;
            for (int i = 0; i < values.Length; i++)
                weight += Convert.ToInt32(values[i])*prob.Weights[i];

            fitness = weight;
            if (!validity)
                fitness = weight/2;// 
            //fitness = fitness -prob.MaxWeight;
            fitness *= prob.SatisfiedCount(this)* prob.MaxWeight;
        }


    }
}
