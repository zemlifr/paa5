﻿using System;
using System.IO;
using System.Linq;

namespace _3SAT
{
    public class TSatProblem
    {
        private TClausule [] clausules;
        private int[] weights;
        private int size;
        private int maxWeight;

        public TSatProblem()
        { 
        }

        public int CCount()
        {
            return clausules.Length;
        }

        private TSatProblem(TClausule[] clausules, int[] weights)
        {
            this.clausules = clausules;
            this.weights = weights;
            size = weights.Length;
            maxWeight = Weights.Sum();
        }

        public int Size => size;

        public int[] Weights
        {
            get { return weights; }
        }

        public int MaxWeight
        {
            get { return maxWeight; }
        }

        public int Weight(TSatSolution sol)
        {
            int sum = 0;
            for (int i = 0; i < sol.Values.Length; i++)
            {
                if (sol.Values[i])
                    sum += weights[i];
            }

            return sum;
        }

        public bool Validate(TSatSolution sol)
        {
            foreach (var clausule in clausules)
            {
                bool res = false;
                foreach (var vb in clausule.Variables)
                {
                    if (vb < 0)
                    {
                        var idx = Math.Abs(vb);
                        if (!sol.Values[idx-1])
                        {
                            res = true;
                            break;
                        }
                    }
                    else
                    {
                        if (sol.Values[vb-1])
                        {
                            res = true;
                            break;
                        }
                    }
                }
                if (!res)
                    return false;
                
                res = false;
            }
            return true;
        }

        public int SatisfiedCount(TSatSolution sol)
        {
            int satisfied = 0;
            foreach (var clausule in clausules)
            {
                bool res = false;
                foreach (var vb in clausule.Variables)
                {
                    if (vb < 0)
                    {
                        var idx = Math.Abs(vb);
                        if (!sol.Values[idx - 1])
                        {
                            res = true;
                            break;
                        }
                    }
                    else
                    {
                        if (sol.Values[vb - 1])
                        {
                            res = true;
                            break;
                        }
                    }
                }
                if (res)
                    satisfied++;

                res = false;
            }
            return satisfied;
        }

        private class TClausule
        {
            private int[] variables = new int[3];

            public TClausule(int a, int b, int c)
            {
                variables[0] = a;
                variables[1] = b;
                variables[2] = c;
            }

            public int[] Variables
            {
                get{return variables;}
            }


        }

        public static TSatProblem FromText(TextReader reader)
        {
            int[] weights = new int[1];
            TClausule[] clausules = new TClausule[1];
            string line;
            int cc = 0;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("c"))
                {} //do nothing;
                else if (line.StartsWith("p"))
                {
                    var words = line.Split(' ');
                    int wsize = int.Parse(words[2]);
                    int csize = int.Parse(words[3]);
                    weights = new int[wsize];
                    clausules = new TClausule[csize];
                }
                else if (line.StartsWith("w"))
                {
                    var words = line.Split(' ');
                    for (var i = 1; i < weights.Length; i++)
                        weights[i] = int.Parse(words[i]);
                }
                else
                {
                    var words = line.Split(' ');
                    int a = int.Parse(words[0]);
                    int b = int.Parse(words[1]);
                    int c = int.Parse(words[2]);
                    clausules[cc] = new TClausule(a,b,c);
                    cc++;
                }
            }

            return new TSatProblem(clausules,weights);
        }

    }

}
