﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace _3SAT
{
    internal class BruteforceSolver
    {
        public int Solve(TSatProblem prob)
        {
            int max = 0;
            bool[] b = new bool[25];
            for (int i = 0; i < 33554432; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    b[j] = (i & (1 << j)) != 0;
                }
                var sol = new TSatSolution(b);
                sol.ComputeFitness(prob);
                if (prob.Validate(sol) && sol.Weight > max)
                    max = sol.Weight;
            }
            return max;
        }
    }
}
