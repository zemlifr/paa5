﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.ExceptionServices;
using System.Xml;

namespace _3SAT
{
    class SatSolver
    {
        private TSatProblem problem;
        Random rand = new Random(DateTime.Now.Millisecond);

        public SatSolver(TSatProblem problem)
        {
            this.problem = problem;
        }

        public int Solve(int popSize=50, int maxCycles=50000, int mutation = 5,bool validOnly=false, int predictedMax =1000)
        {
            List<TSatSolution> population = InitPopulation(popSize);

            int elite = 1;//popSize/20+1;
            int max = 0;
            int maxFitness = -problem.MaxWeight;
            TSatSolution best = new TSatSolution(problem.Size);
            int cattastropheCounter = 0;
            int i = 0;
            for (;i < maxCycles; i++)
            {
                List<TSatSolution> newPop = new List<TSatSolution>();
                population.Sort(new Comparer());
                if (population[0].Fitness > maxFitness)
                {
                    maxFitness = population[0].Fitness;
                    best = population[0];
                    //Console.WriteLine(i + " " + best.Weight + " " + best.Validity);
                }
                if (best.Weight >= predictedMax && best.Validity)
                    return i;

                if (cattastropheCounter > maxCycles/4 && !best.Validity)
                {
                    cattastropheCounter = 0;
                   // TSatSolution valid = FindValid(population);
                    population = InitPopulation(popSize);
                   /* if (valid != null)
                    {
                        population.RemoveAt(population.Count - 1);
                        population.Add(valid);
                    }*/
                    population.Sort(new Comparer());
                    best = new TSatSolution(problem.Size);
                    maxFitness = -problem.MaxWeight;
                }

                    for (int j = 0; j < popSize/2; j++)
                {
                    TSatSolution a = population[j];
                    TSatSolution b = population[popSize-j-1];

                    var breed = Crossover(a, b);

                    newPop.Add(a);
                    newPop.Add(b);
                    newPop.Add(breed.Item1);
                    newPop.Add(breed.Item2);
                    //newPop.Add(breed.Item1.Fitness>breed.Item2.Fitness? breed.Item1:breed.Item2);
                }
                //newPop.RemoveAt(newPop.Count-1);
                //newPop.Add(population[0]);
                newPop.Sort(new Comparer());
                for (int l = elite; l < newPop.Count; l++)
                {
                    if (rand.Next(0, 101) < mutation)
                        Mutate(newPop[l]);
                }
                newPop.RemoveRange(popSize,newPop.Count-popSize);

                if (newPop[0].Validity && validOnly)
                {
                   // Console.WriteLine(newPop[0].Weight);
                    return i;
                }
                population = newPop;
                cattastropheCounter++;
            }
            
            population.Sort(new Comparer());
            //Console.WriteLine(problem.SatisfiedCount(population[0]));
            Console.WriteLine(population[0].Validity + " " + population[0].Weight);
            return population[0].Weight;
            //return i;
        }

        private TSatSolution FindValid(List<TSatSolution> population)
        {
            return population.FirstOrDefault(ind => ind.Validity);
        }

        private List<TSatSolution> InitPopulation(int size)
        {
            List<TSatSolution> population = new List<TSatSolution>();
            for (var i = 0; i < size; i++)
            {
                var sol = new TSatSolution(problem.Size);
                sol.Validity = problem.Validate(sol);
                sol.ComputeFitness(problem);
                population.Add(sol);
            }

            return population;
        }

        protected void Mutate(TSatSolution sol)
        {

            for (int i = 0; i < sol.Values.Length; i++)
                if (rand.NextDouble() < 1.0 / sol.Values.Length)
                    sol.Values[i] = !sol.Values[i];

            sol.Validity = problem.Validate(sol);
            sol.ComputeFitness(problem);
        }

        protected Tuple<TSatSolution, TSatSolution> Crossover(TSatSolution a, TSatSolution b)
        {
            TSatSolution u = new TSatSolution(problem.Size);
            TSatSolution v = new TSatSolution(problem.Size);

            var idx = rand.Next(1, a.Values.Length - 1);
            Array.Copy(a.Values, 0, u.Values, 0, idx);
            Array.Copy(b.Values, 0, v.Values, 0, idx);

            Array.Copy(a.Values, idx, v.Values, idx, a.Values.Length - idx);
            Array.Copy(b.Values, idx, u.Values, idx, b.Values.Length - idx);

            u.Validity = problem.Validate(u);
            v.Validity = problem.Validate(v);
            u.ComputeFitness(problem);
            v.ComputeFitness(problem);

            return new Tuple<TSatSolution, TSatSolution>(u, v);
        }

        class Comparer : IComparer<TSatSolution>
        {
            public int Compare(TSatSolution x, TSatSolution y)
            {
                var a = x.Fitness;
                var b = y.Fitness;
                return -a.CompareTo(b);
            }
        }
    }
}
